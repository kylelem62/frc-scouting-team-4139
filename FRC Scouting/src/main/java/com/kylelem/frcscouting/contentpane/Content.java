package com.kylelem.frcscouting.contentpane;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

/**
 * An abstract type class. Think of it as my equivalent to the {@link android.app.Activity} class
 * from Android.
 *
 * @author Kyle Lemmon
 */
public abstract class Content {
    /**
     * This {@link android.view.View} will fill the entire content pane in the
     * {@link com.kylelem.frcscouting.StartActivity}.
     */
    View parentView;

    /**
     * Creates a {@link Content} object from the inflated xml layout indicated by its resource ID.
     *
     * @param parentViewId Resource ID of layout that content object is tied to.
     */
    public Content(int parentViewId){

        //Using the Context stored in the ContentManager to create LayoutInflater object
        LayoutInflater li = (LayoutInflater)ContentManager.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Setting the parentView equal to the inflated xml layout
        parentView = li.inflate(parentViewId,null);
    }
}
