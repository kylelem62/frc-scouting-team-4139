package com.kylelem.frcscouting.contentpane;

import com.kylelem.frcscouting.R;

/**
 * Home page on the nav menu
 * @author Kyle Lemmon
 * @date 6/24/14
 */
public class ContentWelcome extends Content {
    public ContentWelcome(){
        super(R.layout.content_welcome);

    }
}
