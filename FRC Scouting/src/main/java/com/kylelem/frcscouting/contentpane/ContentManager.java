package com.kylelem.frcscouting.contentpane;

import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Static-only object, <code>ContentManager</code> is used to control the content pane of screen.
 * @author Kyle Lemmon
 * @date 6/24/14
 */
public class ContentManager {

    private static Context context;

    private static RelativeLayout hostLayout;


    /**
     * Must be called before any other interaction with the Content Pane or with this class
     * @param c Context of application
     * @param layout This is the layout of the content pane. This can not be cleanly determined
     *               from this class, and must be passed at initiation.
     */
    public static void init(Context c,RelativeLayout layout){
        context = c;
        hostLayout = layout;
    }

    /**
     * Not sure if you are supposed to do this, but this method makes the application context
     * accessible from all classes in the app, for convenience's sake.
     * @return Context of {@link com.kylelem.frcscouting.StartActivity}.
     */
    public static Context getContext()
    {
        return context;
    }

    /**
     * Equivalent to Android's startIntent(Intent) method. This cancels the old content object, and
     * creates and displays the new one.
     * @param newContent Already initialized content pane to be displayed.
     */
    public static void startNewContent(Content newContent) {
        hostLayout.removeAllViews();
        newContent.parentView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,RelativeLayout.LayoutParams.FILL_PARENT));
        RelativeLayout parentParent = (RelativeLayout)newContent.parentView.getParent();
        if (parentParent!=null)
            parentParent.removeAllViews();
        hostLayout.addView(newContent.parentView);

        hostLayout.invalidate();
    }
}
