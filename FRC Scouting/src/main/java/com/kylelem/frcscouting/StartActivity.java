package com.kylelem.frcscouting;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.kylelem.frcscouting.contentpane.ContentManager;

/**
 * This is the main activity. The application never leaves this activity, instead user action
 * causes views to be switched out of the content pane here. The navigation menu on the left side
 * has all subcategories shown there.
 * <p>
 * The hardcoded contents of the navigation bar are stored in
 * {@link com.kylelem.frcscouting.NavMenuAdapter#getNavMenu(android.content.Context)}.
 * @author Kyle Lemmon
 * @date 6/24/14
 */
public class StartActivity extends ActionBarActivity {
    /**
     * I like to add these to every Activity. It just makes life easier for me
     */
    public final static String INTENT = "com.kylelem.frcscouting.main";

    /**
     * All initiation code for the content panes is done here.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        RelativeLayout fl = (RelativeLayout)findViewById(R.id.content_frame);
        ContentManager.init(getApplicationContext(), fl);
        NavMenuAdapter navMenuAdapter = NavMenuAdapter.getNavMenu(getApplicationContext());
        ListView navMenuView = (ListView) findViewById(R.id.nav_menu_view);
        navMenuView.setAdapter(navMenuAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
