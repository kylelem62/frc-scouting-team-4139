package com.kylelem.frcscouting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.kylelem.frcscouting.contentpane.Content;
import com.kylelem.frcscouting.contentpane.ContentManager;
import com.kylelem.frcscouting.contentpane.ContentWelcome;

/**
 * This class contains a static object of itself
 * ({@link com.kylelem.frcscouting.NavMenuAdapter#navMenuAdapter}), this is the only object, as
 * there is only one navigation menu throughout the entire app.
 * @author Kyle Lemmon
 * @date 6/24/2014
 */
public class NavMenuAdapter extends ArrayAdapter<NavItem> {
    /**
     * This should be the only NavMenuAdapter.
     */
    private static NavMenuAdapter navMenuAdapter;

    private LayoutInflater layoutInflater;

    public NavMenuAdapter(Context mContext, int layoutResourceId, NavItem[] data) {
        super(mContext, layoutResourceId, data);

        layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Fetches the NavMenuAdapter object used by the navigation menu ListView
     * @param context Context of the current activity (hopefully only
     * {@link com.kylelem.frcscouting.StartActivity}).
     * @return NavMenuAdapter
     */
    public static NavMenuAdapter getNavMenu(Context context) {
        if(navMenuAdapter ==null) {
            NavItem[] data = new NavItem[3];
            data[0] = new NavItem("Home", new ContentWelcome(),context);
            data[1] = new NavItem("Test", new ContentWelcome(),context);
            data[2] = new NavItem("Another one here", new ContentWelcome(),context);
            navMenuAdapter = new NavMenuAdapter(context, R.id.nav_menu_view, data);
        }
        return navMenuAdapter;
    }

    /**
     * Overriding ListViewAdapter's getView, so we can construct our own views here. This allows
     * for many different possibilities in the nav menu.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView = layoutInflater.inflate(R.layout.nav_menu_child,parent,false);
        }
        TextView view = (TextView) convertView.findViewById(R.id.nav_menu_text);
        view.setText(getItem(position).name);
        convertView.setOnClickListener(getItem(position).listener);
        return convertView;
    }
}

/**
 * Container for the options on the navigation bar.
 * TODO create option for rich layouts to exist in the nav bar
 */
class NavItem {
    String name;
    Content content;
    Context context;
    public NavItem(String text, Content content, Context c)
    {
        name = text;
        this.content = content;
        context = c;
    }
    public View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ContentManager.startNewContent(content);
            Toast.makeText(context,"Onclick",Toast.LENGTH_SHORT).show();
    }
    };
}